import React from "react";
import './Boton.css'


const Boton = (props) => {
  const button_onClick = () =>{
    if(props.buttonClick==='equals')
      return props.equals;
    if(props.buttonClick==='backSpace')
    return props.backSpace;
    if(props.buttonClick==='clear')
    return props.clear;
    return props.buttonClick(props.btnValue);
  }
  return (
  <button className={props.btnclass} onClick={button_onClick}>{props.btnValue}</button>  
  );
};

export default Boton;
