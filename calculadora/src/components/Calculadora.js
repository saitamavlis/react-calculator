import React, { useState } from "react";
import Boton from "./Boton";
import Display from "./Display";
import Record from "./Record";
import "./Calculadora.css";

const Calculadora = () => {
  const [result, setResult] = useState("");
  const [operation, setOperation] = useState("");
  const [record, setRecord] = useState("");

  const clickedButton = (button) => {
    setResult(result.concat(button));
    setOperation(result.concat(button));
  };

  const equals = () => {
    try {
      if (result !== "") {
        setResult(
          eval(result)
            .toLocaleString("en-US", {
              maximumFractionDigits: 2,
              minimumFractionDigits: 0,
            })
            .replace(/([.,])(\d\d\d\D|\d\d\d$)/g, "$2")
        );
        const newrecord = operation.concat("=").concat(
          eval(result)
            .toLocaleString("en-US", {
              maximumFractionDigits: 2,
              minimumFractionDigits: 0,
            })
            .replace(/([.,])(\d\d\d\D|\d\d\d$)/g, "$2")
        );
        setRecord(record.concat(newrecord + "\n"));
        setOperation("");
      }
    } catch (e) {
      setResult("Error");
    }
    ///setResult(eval(result).toLocaleString('es-AR', { maximumFractionDigits: 2, minimumFractionDigits: 0 }));
  };
  const clear = () => {
    setResult("");
  };

  const backSpace = () => {
    setResult(result.slice(0, -1));
  };

  console.log(record);
  return (
    <div className="container">
      <div className='calculator'>
        <h1>Calculadora</h1>
        <div>
          <Display displayText={result} />
        </div>
        <div className="keypad">
          <Boton btnValue="7" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="8" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="9" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="/" btnclass="operator" buttonClick={clickedButton} />
          <Boton btnValue="4" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="5" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="6" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="*" btnclass="operator" buttonClick={clickedButton} />
          <Boton btnValue="3" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="2" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="1" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="-" btnclass="operator" buttonClick={clickedButton} />
          <Boton btnValue="0" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="00" btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="." btnclass="number" buttonClick={clickedButton} />
          <Boton btnValue="+" btnclass="operator" buttonClick={clickedButton} />
          <Boton btnValue="clear" btnclass="clear" buttonClick={clear} />
          <Boton btnValue="C" btnclass="backSpace" buttonClick={backSpace} />
          <Boton btnValue="=" btnclass="equals" buttonClick={equals} />
        </div>
      </div>
      <div className='operations'>
        <h3>Operaciones</h3>
        <Record record={record} />
      </div>
    </div>
  );
};

export default Calculadora;
