import React from "react";
import "./Display.css";

const Display = (props) => {
  
  return (
    <>
    <div className='display'>
        <form>
            <input type='text' defaultValue={props.displayText} readOnly='readOnly' />
        </form>
    </div>
    </>
  );
};

export default Display;